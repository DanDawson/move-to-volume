# move-to-volume

A bash script to automate the process of moving a directory from a Digital Ocean server to an external volume.

Basic steps to use:

- Create a new Volume on DigitalOcean
- Install the bash script (I put it in /usr/local/bin/ since that was already in my $PATH)
- Navigate to the parent folder of the one you want to move
- Run the move-to-volume.sh script
- Enter the name of the folder that you plan to move
- Enter the full path of the Target Folder that will be newly created
- Review the commands that are going to be issued and see if they look accurate. If so, proceed to run the script
- Test the site and verify everything works as expected. If it does, run the command to delete the old copy of the files on the server

**Note:**
Running files from external volumes are slower than they would be if loaded directly from the SSD. The reason for the reduction in speed is that the Volumes are accessed across the network rather than locally connected.
