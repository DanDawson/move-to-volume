#!/bin/bash
# This script is designed to assist in moving folders from the primary server drive
# to an external DigitalOcean Volume, creating a symbolic link to it, and updating
# permissions to match what they were before the move.
# by Dan Dawson (2020)
# Edited 2020-01-07

# Make sure this script is run as root
if [ "$EUID" -ne 0 ]
then 
    echo ""
        echo "Please run this script as root."
        exit
fi

echo "Is $PWD the correct path which contains the directory to be moved? "
select yn in "Yes" "No"; do
    case $yn in
        Yes ) break;;
        No ) echo 'Please change to the correct path and try again.'; exit;;
    esac
done

read -p 'Source folder that is going to be moved (Example: public): ' SOURCE_FOLDER

USER=$(stat -c '%U' $SOURCE_FOLDER)
GROUP=$(stat -c '%G' $SOURCE_FOLDER)

read -p 'Full path to folder to be CREATED on the new volume (Example: /mnt/sitename/foldername): ' TARGET_FOLDER

echo ""
echo 'SAMPLE OUTPUT:'
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="

echo "mkdir $TARGET_FOLDER"
echo "mv $SOURCE_FOLDER $SOURCE_FOLDER-old"
echo "ln -s $TARGET_FOLDER $PWD/$SOURCE_FOLDER"
echo "cp -a $PWD/$SOURCE_FOLDER-old/. $TARGET_FOLDER"
echo "chown -hR $USER:$GROUP $TARGET_FOLDER"
echo "chown -h $USER:$GROUP $PWD/$SOURCE_FOLDER"
echo ""
echo ""

echo "If the above looks accurate, would you like to proceed? "
select yn in "Yes" "No"; do
    case $yn in
        Yes ) break;;
        No ) echo 'Please try again with correct values.'; exit;;
    esac
done

# Execute the commands:
mkdir $TARGET_FOLDER
mv $SOURCE_FOLDER $SOURCE_FOLDER-old
ln -s $TARGET_FOLDER $PWD/$SOURCE_FOLDER
cp -a $PWD/$SOURCE_FOLDER-old/. $TARGET_FOLDER
chown -hR $USERNAME:$USERNAME $TARGET_FOLDER
chown -h $USERNAME:$USERNAME $PWD/$SOURCE_FOLDER

echo ""
echo "Now you can test the site, and if everything works you can run the following command:"
echo "rm -rf $SOURCE_FOLDER-old"

exit
